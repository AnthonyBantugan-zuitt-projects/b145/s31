const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);
let users = [
	{
		username: "johndoe",
		password: "johndoe1234",
	},
];

// Solution #1 and #2
app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});

// Solution #3 and #4
app.get("/users", (req, res) => {
	res.send(req.body);
});

// Solution #5 and #6
app.delete("/delete-user", (req, res) => {
	users.some((user) => {
		if (user.username === req.body.username) {
			res.send(`User ${user.username} has been deleted`);
		}
	});
});

app.listen(port, () => console.log(`Server running at port ${port}`));
