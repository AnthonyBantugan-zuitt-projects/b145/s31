// Creating a simple server in Node.js

// let http = require('http')
// http.createServer((req, res) => {
// 	if(req.url == "/" && req.method == "GET") {
// 		res.writeHead(200, {'Content-Type': "text/plain"})
// 		res.end("Data Received")
// 	}
// }).listen(4000)

// ====================================================
// Creating a simple server in Express.js

const express = require("express");
const app = express();
const port = 4000;

// Middlewares
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

// mock database
let users = [
	{
		email: "attackTitan@mail.com",
		username: "thisIsEren",
		password: "notABird",
		isAdmin: false,
	},
	{
		email: "mrClean@mail.com",
		username: "AckermanLevi",
		password: "stillAlive",
		isAdmin: true,
	},
	{
		email: "redScarf@mail.com",
		username: "Mikasa12",
		password: "whereIsEren",
		isAdmin: false,
	},
];

let loggedUser;

app.get("/", (req, res) => {
	res.send("Hello World");
});
app.get("/hello", (req, res) => {
	res.send("Hello from batch 145");
});
app.post("/", (req, res) => {
	console.log(req.body);
	res.send(
		`Hello, I am ${req.body.name}, my age is ${req.body.age}. I could be described as ${req.body.description} `
	);
});

// Register route
app.post("/users", (req, res) => {
	console.log(req.body); //check data

	let newUser = {
		email: req.body.email,
		userName: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin,
	};

	users.push(newUser); //add newUser to users variable/array
	console.log(users);
	res.send(`User ${req.body.username} has successfully registered`);
});

// log in route
app.post("/users/login", (req, res) => {
	console.log(req.body);

	let foundUser = users.find((user) => {
		return (
			user.username === req.body.username &&
			user.password === req.body.password
		);
	});

	if (foundUser !== undefined) {
		let foundUserIndex = users.findIndex((user) => {
			return user.username === foundUser.username;
		});

		foundUser.index = foundUserIndex;

		loggedUser = foundUser;

		res.send(`Thank you for logging in.`);
	} else {
		loggedUser = foundUser;
		res.send("Sorry, wrong credentials");
	}
});

// Change password

app.put("/users/change-password", (req, res) => {
	let message;

	for (let i = 0; i < users.length; i++) {
		if (req.body.username == users[i].username) {
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been changed`;

			break;
		} else {
			message = `User does not exist`;
		}
	}

	res.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));
